@RestResource(urlMapping='/Accounts/*/contacts')
global with sharing class AccountManager{

    @HttpGet
    global static Account getAccount(){
        RestRequest request = RestContext.request;
        String[] uriStack = request.requestURI.split('/');
        String id = uriStack[uriStack.size() - 2];
        System.debug('DebugMe: ' + id);
        Account a = [SELECT Id, Name, (SELECT Id, Name FROM Contacts WHERE AccountId = :id) FROM Account WHERE Id = :id];
        return a;
    }

}