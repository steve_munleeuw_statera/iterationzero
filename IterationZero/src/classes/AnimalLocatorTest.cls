@isTest
public class AnimalLocatorTest{
    
    @isTest
    public static void testGetAnimalNameById(){
//        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
//        mock.setStaticResource('getAnimalNameByIdResource');
//        mock.setStatusCode(200);
//        mock.setHeader('Content-Type', 'application/json;charset=UTF-8');
//        Test.setMock(HttpCalloutMock.class, mock);

        Test.setMock(HttpCalloutMock.class, new AnimalLocatorMock());
        
        
        String result = AnimalLocator.getAnimalNameById(1);
        
        System.assertEquals('chicken', result);
    
    }
}